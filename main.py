import torchvision
import torch
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import time
import copy
import sys
import sklearn
from datetime import datetime
from sklearn.metrics import confusion_matrix
import numpy

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
# device = torch.device("cpu")


def train_model(net, data_loaders, epochs):
    print(f"Device: {device}")
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=0.05, momentum=0.9)

    error_table = {'train': [], 'eval': []}
    time_table = {'train': [], 'eval': []}

    for epoch in range(epochs):  # loop over the dataset multiple times

        for phase in data_loaders.keys():
            if phase == 'train':
                model.train()
            else:
                model.eval()

            running_loss = 0.0
            start_time = time.time()
            for i, data in enumerate(data_loaders[phase], 0):
                # print progress
                if i % 10 == 0:
                    sys.stdout.write(f"\rEpoch[{epoch}], {phase}: Progress: {(i / len(data_loaders[phase])): .3f}")

                # get the inputs; data is a list of [inputs, labels]
                inputs, labels = data[0].to(device), data[1].to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                with torch.set_grad_enabled(phase == 'train'):
                    # forward + backward + optimize
                    outputs = net(inputs)
                    loss = criterion(outputs, labels)

                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                    # adjust loss
                    running_loss += loss.item()

            epoch_loss = running_loss / len(data_loaders[phase])
            error_table[phase].append(epoch_loss)

            elapsed_time = (time.time() - start_time)
            sys.stdout.write(f"\rEpoch[{epoch}], {phase}: time: {elapsed_time: .3f}s\n")
            time_table[phase].append(elapsed_time)

        # print(f"Epoch[{epoch}] loss: {running_loss / len(train_loader.dataset)}")

    print('Finished Training')
    return error_table, time_table


def prepare_model(model_type):

    if model_type == "1a":
        model = torchvision.models.mobilenet_v2(pretrained=True)
        for param in model.parameters():
            param.requires_grad = False
        model.classifier[1] = torch.nn.Linear(in_features=model.classifier[1].in_features, out_features=131)
        model.to(device)

    elif model_type == "2a":
        model = torchvision.models.mobilenet_v2(pretrained=True)
        for param in model.parameters():
            param.requires_grad = False

        for param in model.features[18][0].parameters():
            param.requires_grad = True

        model.classifier[1] = torch.nn.Linear(in_features=model.classifier[1].in_features, out_features=131)
        model.to(device)

    elif model_type == "2b":
        model = torchvision.models.mobilenet_v2(pretrained=True)
        for param in model.parameters():
            param.requires_grad = False

        for param in model.features[18][0].parameters():
            param.requires_grad = True

        for param in model.features[17].conv[2].parameters():
            param.requires_grad = True

        model.classifier[1] = torch.nn.Linear(in_features=model.classifier[1].in_features, out_features=131)
        model.to(device)

    elif model_type == "2c":
        model = torchvision.models.mobilenet_v2(pretrained=False)
        model.classifier[1] = torch.nn.Linear(in_features=model.classifier[1].in_features, out_features=131)
        model.to(device)

    elif model_type == "2c-trained":
        model = torchvision.models.mobilenet_v2(pretrained=True)
        model.classifier[1] = torch.nn.Linear(in_features=model.classifier[1].in_features, out_features=131)
        model.to(device)


    return model


def measure_batch_times():
    for i in [1, 4, 16, 64, 256]:
        train_dataset = torchvision.datasets.ImageFolder("./fruits-360/Training", transform=mobilenet_transform)
        train_loader = {'train': torch.utils.data.DataLoader(train_dataset, batch_size=i, shuffle=True, num_workers=6)}
        start_time = time.time()
        train_model(prepare_model("1a"), train_loader, 1)
        print(f'Batch size[{i}]: {time.time() - start_time}')


def dict_to_csv(dictionary):
    s = ','.join(dictionary.keys()) + '\n'
    for i in range(len(dictionary['train'])):
        s += ','.join([str(dictionary['train'][i]), str(dictionary['eval'][i])]) + '\n'
    print(s)
    return s


def save_dict_to_file_csv(dict, task):
    csv_file = open('./results/' + task + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + '.csv', 'w')
    csv_file.write(dict_to_csv(dict))
    csv_file.close()


def get_test_classification(model, dataset):
    model.eval()
    dataset_loader = torch.utils.data.DataLoader(dataset, batch_size=64, shuffle=False, num_workers=6)
    predictions = torch.tensor([], device=device)
    correct = torch.tensor([], device=device)
    for i, data in enumerate(dataset_loader, 0):
        inputs, labels = data[0].to(device), data[1].to(device)
        if i % 10 == 0:
            print(i)

        with torch.no_grad():
            outputs = model(inputs)
            _, max_idx = torch.max(outputs, 1)
            predictions = torch.cat((predictions, max_idx), dim=0)
            correct = torch.cat((correct, labels), dim=0)

    return predictions, correct


def read_model(model_name, model_type):
    model = prepare_model(model_type)
    model.load_state_dict(torch.load('./models/' + model_name))
    return model


def create_conf_matrix(model_name, task, dataset):
    model = read_model(model_name, task)
    preds, correct = get_test_classification(model, dataset)
    preds, correct = preds.detach().cpu().numpy(), correct.detach().cpu().numpy()
    conf_mat = confusion_matrix(correct, preds)
    numpy.savetxt("./results/confmat" + task + datetime.now().strftime("%Y-%m-%d_%H-%M-%S") + '.csv', conf_mat, delimiter=',')


if __name__ == '__main__':
    # load "Fruits 360" dataset:
    mobilenet_transform = transforms.Compose([
        transforms.Resize(254),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])

    main_loaders = {}
    print("Create train_loader...")
    train_dataset = torchvision.datasets.ImageFolder("./fruits-360/Training", transform=mobilenet_transform)
    main_loaders['train'] = torch.utils.data.DataLoader(train_dataset, batch_size=64, shuffle=False, num_workers=6)

    print("Create data_loader...")
    test_dataset = torchvision.datasets.ImageFolder("./fruits-360/Test", transform=mobilenet_transform)
    main_loaders['eval'] = torch.utils.data.DataLoader(test_dataset, batch_size=64, shuffle=True, num_workers=6)

    current_task = '2c-trained'

    # TRAIN MODEL
    model = prepare_model(current_task)
    # # print("train_model...")
    # result, times = train_model(model, main_loaders, 30)
    # save_dict_to_file_csv(result, current_task)
    # save_dict_to_file_csv(times, current_task + "-time")
    # torch.save(model.state_dict(), "./models/" + current_task + "-" + datetime.now().strftime("%Y-%m-%d_%H-%M-%S"))

    # CREATE CONFUSION MATRIX
    # create_conf_matrix("2c-trained-2021-01-29_13-02-36", current_task, test_dataset)
